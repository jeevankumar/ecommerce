# Ecommerce app
ecommerce app for sellers, and customers, to setup their products and purchase to, 
currently only add your products and create your ordre is implemented, to meet the requirements of the assignment, in future, more features will be added, 

### Dependecies
1. nodejs
2. expressjs
3. mongoose
4. jsonwebtoken
5. bcrypt
6. bodyparser
7. morgan

### Application Tier
##### backend
1. nodejs
2. expressjs
3. mongodb
##### front end
1. angular
2. ngx-bootstrap


### Set Up Your Mongodb
go to file ```nodemon.js```, change the value of ```MONGODB_URL``` 

```
{
    "env" : {
        "MONGODB_URL": "<mongodbclusterurl>",
        ...
    }
}
```

### Configuration
open the cmd inside the root directory of the project then run the following two commands
1. ```npm install```
2. ```npm start```

