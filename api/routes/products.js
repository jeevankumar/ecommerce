const express = require('express');
const router = express.Router();
const uuid = require('uuid');
const Product = require('../../model/product');
const Order = require('../../model/order');
const OrderStatus = require('../../model/order.status');
const common = require('../../common');
const mongoose = require('mongoose');
const order = require('../../model/order');
const checkauth = require('../checkauth');

const sendResponse = common.sendResponse;

router.get('/', checkauth, (req, res, next) => {
    sendResponse(Product.find(), res);
})

router.get('/:id', checkauth, (req, res, next) => {
    sendResponse(Product.findById(req.params.id), res);
})

router.post('/', checkauth, (req, res, next) => {
    let responseData = null;

    let product = new Product({
        title: req.body.title,
        price: req.body.price,
        user: req.userData.email
    });

    if (req.body._id) {
        promise = Product.updateOne({ _id: req.body._id }, { $set: product });
    } else {
        product._id = new mongoose.Types.ObjectId()
        promise = product.save();
    }
    sendResponse(promise, res);
});

router.post('/addToOrder', checkauth, (req, res, next) => {
    try {
        if (req.body && req.body.productId) {
            Product.findById(req.body.productId).then(p => {
                let orderProduct = {
                    _id: p._id,
                    title: p.title,
                    price: p.price,
                    quantity: req.body.quantity | 1
                };
                let newOrder = new Order();
                Order.findOne({ status: OrderStatus.INITIATED, userId: null }).then(o => {
                    if (o != null) {
                        let existingProduct = o.products.filter(v => v._id.equals(orderProduct._id));
                        newOrder.products = o.products.filter(v => !v._id.equals(orderProduct._id));
                        if (existingProduct != null && existingProduct.length > 0) {
                            existingProduct = existingProduct[0];
                            existingProduct.quantity += orderProduct.quantity;
                            newOrder.products.push(existingProduct);
                        } else {
                            newOrder.products.push(orderProduct);
                        }
                        sendResponse(Order.updateOne({ _id: o._id }, { $set: { products: newOrder.products } }), res);

                    } else {
                        newOrder._id = mongoose.Types.ObjectId();
                        newOrder.products = [orderProduct];
                        newOrder.status = OrderStatus.INITIATED;
                        newOrder.user = req.userData.email;
                        sendResponse(newOrder.save(), res);
                    }
                })
                    // if order not found
                    .catch(err => {
                        res.status(500).json({ error: err });
                    })
            })
                // if product not found
                .catch(err => {
                    res.status(500).json({ error: err });
                })
        }
        // if product id not in request body 
        else {
            res.status(500).json({ error: 'Product Id is required' });
        }
    } catch (err) {
        res.status(500).json({ error: err });
    }
});

router.post('/updateOrder', checkauth, (req, res, next) => {
    if (req.body && req.body.productId) {
        Product.findById(req.body.productId).then(p => {
            let orderProduct = {
                _id: p._id,
                quantity: req.body.quantity || 1
            };
            // console.log("orderProduct: " + orderProduct);
            // console.log("orderProduct.quantity: " + orderProduct.quantity);
            let newOrder = new Order();
            Order.findOne({ status: OrderStatus.INITIATED, userId: null }).then(o => {
                if (o != null) {
                    console.log("before product update: " + o.products)
                    o.products.forEach(v => {
                        if (v._id.equals(orderProduct._id)) {
                            v.quantity = orderProduct.quantity
                        }
                    })
                    console.log("after product update: " + o.products)
                    newOrder.products = o.products
                    sendResponse(Order.updateOne({ _id: o._id }, { $set: { products: newOrder.products } }), res);

                } else {
                    newOrder._id = mongoose.Types.ObjectId();
                    newOrder.products = [orderProduct];
                    newOrder.status = OrderStatus.INITIATED;
                    newOrder.userId = null;
                    sendResponse(newOrder.save(), res);
                }
            })
                // if order not found
                .catch(err => {
                    res.status(500).json({ error: err });
                })
        })
            // if product not found
            .catch(err => {
                res.status(500).json({ error: err });
            })
    }
    // if product id not in request body 
    else {
        res.status(500).json({ error: 'Product Id is required' });
    }
});

router.post('/removeFromOrder', checkauth, (req, res, next) => {
    if (req.body && req.body.productId) {
        Product.findById(req.body.productId).then(p => {
            let orderProduct = { _id: p._id };
            let newOrder = new Order();
            Order.findOne({ status: OrderStatus.INITIATED, userId: null }).then(o => {
                if (o != null) {
                    newOrder.products = o.products.filter(v => !v._id.equals(orderProduct._id));
                    sendResponse(Order.updateOne({ _id: o._id }, { $set: { products: newOrder.products } }), res);
                } else {
                    res.status(200).json({ message: "Product not available in this order " + o._id })
                }
            })
                // if order not found
                .catch(err => {
                    res.status(500).json({ error: err });
                })
        })
            // if product not found
            .catch(err => {
                res.status(500).json({ error: err });
            })
    }
    // if product id not in request body 
    else {
        res.status(500).json({ error: 'Product Id is required' });
    }
});

router.delete('/:id', checkauth, (req, res, next) => {
    sendResponse(Product.findByIdAndRemove({ _id: req.params.id }), res);
})



module.exports = router;