const OrderStatus = {
    INITIATED : "Initiated",
    ORDERED : "Ordered",
    SHIPPED : "Shipped",
    DELIVERED : "Delivered",
    RETURNED : "Returned",
    EXCHANGED : "Exchanged"
};

module.exports = OrderStatus;