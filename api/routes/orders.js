const express = require('express');
const common = require('../../common');
const uuid = require('uuid');
const router = express.Router();
const Order = require('../../model/order');
const OrderStatus = require('../../model/order.status');
const mongoose = require('mongoose');
const checkauth = require('../checkauth');

let sendResponse = common.sendResponse;

router.get('/', checkauth, (req, res, next) => {
    console.log(sendResponse);
    sendResponse(Order.find({user: req.userData.email}), res);
})

router.get('/initiated', checkauth, (req, res, next) => {
    console.log(sendResponse);
    sendResponse(Order.find({status : OrderStatus.INITIATED, user: req.userData.email}), res);
})

router.get('/status', checkauth, (req, res, next) => {
    console.log(OrderStatus);
    res.status(200).json({ status: OrderStatus })
})

router.get('/:id', checkauth,  (req, res, next) => {
    sendResponse(Order.findById(req.params.id), res);
});

router.get('/:id/:status', checkauth, (req, res, next) => {
    if(req.params.id && req.params.status){
        let status = OrderStatus[req.params.status];
        if(status){
            sendResponse(Order.findByIdAndUpdate(req.params.id, {$set : { status : status}}), res);
        }else {
            res.status(500).json({error: "status not found: " + status});
        }
    } else {
        res.status(500).json({error : "order id or status is missing"});
    }
});


router.delete('/:id', checkauth, (req, res, next) => {
    sendResponse(Order.findByIdAndRemove(req.params.id), res);
})

module.exports = router;