const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const fs = require('fs');

const app = express();

const productRoutes = require('./api/routes/products')
const orderRoutes = require('./api/routes/orders')
const userRoutes = require('./api/routes/auth')

mongoose.connect(MONGODB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/', express.static('web-app'));
app.use('/app', (req, res, next) => {
    res.redirect('/');
});

// app.use('/', (req, res, next) => {
//     fs.readFile('./index.html', (err, data) => {
//         if (err) {
//             res.status(500).json({ error: error })
//             console.log(err);
//         } else {
//             res.write(data, werr=> {
//                 console.log( werr );
//             });
//         }
//     })
// });

app.use('/api', (req, res, next) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, PATCH, DELETE');
        res.status(200).json({});
    }
    next();
});

app.use('/api', userRoutes);
app.use('/api/products', productRoutes);
app.use('/api/orders', orderRoutes);

app.use((req, res, next) => {
    console.log('error 404: Not Found');
    const error = new Error("Not Found");
    error.status = 404;
    next(error);
});

app.use((err, req, res, next) => {
    console.log('thrown error' + err);
    // res
    //     .status(err.status || 500);
    // res
    //     .json({
    //         error : {
    //             message : err.message
    //         }
    // });

});

module.exports = app;